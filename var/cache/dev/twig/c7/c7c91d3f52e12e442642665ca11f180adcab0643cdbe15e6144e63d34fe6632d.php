<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SgDatatablesBundle:i18n:fa.json.twig */
class __TwigTemplate_fb0cc553818a07b1b1eb52e7ef6505e4198e1a024f7072fa48700904b4c10959 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SgDatatablesBundle:i18n:fa.json.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SgDatatablesBundle:i18n:fa.json.twig"));

        // line 1
        echo "/**
 * Persian translation
 *  @name Persian
 *  @anchor Persian
 *  @author <a href=\"http://www.chavoshi.com/\">Ehsan Chavoshi</a>
 *  @author <a href=\"http://www.robowiki.ir/\">Mohammad Babazadeh</a>
 */
{
    \"sEmptyTable\":     \"هیچ داده ای در جدول وجود ندارد\",
    \"sInfo\":           \"نمایش _START_ تا _END_ از _TOTAL_ رکورد\",
    \"sInfoEmpty\":      \"نمایش 0 تا 0 از 0 رکورد\",
    \"sInfoFiltered\":   \"(فیلتر شده از _MAX_ رکورد)\",
    \"sInfoPostFix\":    \"\",
    \"sInfoThousands\":  \",\",
    \"sLengthMenu\":     \"نمایش _MENU_ رکورد\",
    \"sLoadingRecords\": \"در حال بارگزاری...\",
    \"sProcessing\":     \"در حال پردازش...\",
    \"sSearch\":         \"جستجو:\",
    \"sZeroRecords\":    \"رکوردی با این مشخصات پیدا نشد\",
    \"oPaginate\": {
        \"sFirst\":    \"ابتدا\",
        \"sLast\":     \"انتها\",
        \"sNext\":     \"بعدی\",
        \"sPrevious\": \"قبلی\"
    },
    \"oAria\": {
        \"sSortAscending\":  \": فعال سازی نمایش به صورت صعودی\",
        \"sSortDescending\": \": فعال سازی نمایش به صورت نزولی\"
    }
}
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SgDatatablesBundle:i18n:fa.json.twig";
    }

    public function getDebugInfo()
    {
        return array (  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("/**
 * Persian translation
 *  @name Persian
 *  @anchor Persian
 *  @author <a href=\"http://www.chavoshi.com/\">Ehsan Chavoshi</a>
 *  @author <a href=\"http://www.robowiki.ir/\">Mohammad Babazadeh</a>
 */
{
    \"sEmptyTable\":     \"هیچ داده ای در جدول وجود ندارد\",
    \"sInfo\":           \"نمایش _START_ تا _END_ از _TOTAL_ رکورد\",
    \"sInfoEmpty\":      \"نمایش 0 تا 0 از 0 رکورد\",
    \"sInfoFiltered\":   \"(فیلتر شده از _MAX_ رکورد)\",
    \"sInfoPostFix\":    \"\",
    \"sInfoThousands\":  \",\",
    \"sLengthMenu\":     \"نمایش _MENU_ رکورد\",
    \"sLoadingRecords\": \"در حال بارگزاری...\",
    \"sProcessing\":     \"در حال پردازش...\",
    \"sSearch\":         \"جستجو:\",
    \"sZeroRecords\":    \"رکوردی با این مشخصات پیدا نشد\",
    \"oPaginate\": {
        \"sFirst\":    \"ابتدا\",
        \"sLast\":     \"انتها\",
        \"sNext\":     \"بعدی\",
        \"sPrevious\": \"قبلی\"
    },
    \"oAria\": {
        \"sSortAscending\":  \": فعال سازی نمایش به صورت صعودی\",
        \"sSortDescending\": \": فعال سازی نمایش به صورت نزولی\"
    }
}
", "SgDatatablesBundle:i18n:fa.json.twig", "/Users/cengizkara/PhpstormProjects/pathproject/vendor/sg/datatablesbundle/Resources/views/i18n/fa.json.twig");
    }
}
